import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router'; 
import { FormGroup, FormControl } from '@angular/forms';
import { UpdateDetailsService } from "./update-details.service";
import { SharedService } from "../core/shared.service";

@Component({
  selector: 'app-update-details',
  templateUrl: './update-details.component.html',
  styleUrls: ['./update-details.component.css']
})
export class UpdateDetailsComponent implements OnInit {
  public formdata;  
  public errorMessage: string = "";
  public successMessage: string = "";
  public home = true;
  public update = false;
  constructor(private router: Router, private updateDetailsService: UpdateDetailsService, private sharedService: SharedService) {  }

  ngOnInit() {
    this.sharedService.CustomerId.subscribe(id => {
        console.log("id "+ id)
        if(id != "default message"){
          this.updateDetailsService.getUserinfo(id)
          .map((response)=>{
            debugger
            console.log("response "+ response)
          })
        }
        
    })
    
    this.formdata = new FormGroup({
      fname: new FormControl(""),
      mname: new FormControl(""),
      lname: new FormControl(""),
      cid: new FormControl(""),
      uname: new FormControl(""),
      passwd: new FormControl(""),
      contactNumber: new FormControl(""),
      email: new FormControl(""),
      address: new FormControl(""),
      upload: new FormControl("")
    });
  }


  onClickSubmit(data) {
    console.log("Inside submit button " + JSON.stringify(data));

  }

  onSignOut(){
    this.router.navigate(['login']);
  }
 
  updateDetails(){
    this.home = false;
    this.update = true;
  }

}


