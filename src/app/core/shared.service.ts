import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SharedService {

    constructor(private http: Http) { }
    private messageSource = new BehaviorSubject('1001');
    CustomerId = this.messageSource.asObservable();

    public sharedCustomerId(id){
        this.messageSource.next(id);
    }
}